/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafkom_2016130023;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 *
 * @author Muhammad Supian
 */
public class LatihanAnimasi2 extends JPanel {

    Thread animasi, repaint;
    int x = 100, y = 100, a = 10, batas = 240;

    public LatihanAnimasi2() {
        setBackground(Color.yellow);
        initThread();
        animasi.start();
        repaint.start();
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        drawText(g2);
    }

    public void initThread() {
        animasi = new Thread(new Runnable() {

            public void run() {
                while (true) {
                    try {
                        Thread.sleep(150);
                    } catch (Exception e) {
                    }
                }
            }
        });

        repaint = new Thread(new Runnable() {

            public void run() {
                while (true) {
                    if (a < batas) {
                        a = a + 15;
                        batas = 240;
                    } else {
                        a = a - 15;
                        batas = 15;
                    }

                    x = (int) (Math.random() * 10);
                    y = (int) (Math.random() * 10);
                    try {
                        Thread.sleep(10);
                    } catch (Exception e) {
                    }
                    SwingUtilities.invokeLater(new Runnable() {

                        public void run() {
                            repaint();
                        }
                    });
                }
            }
        });
    }

    public void drawText(Graphics2D g2) {
        g2.setFont(new Font("Arial", 1, 100));
        g2.setColor(new Color((int) (Math.random() * 250), (int) (Math.random() * 250), (int) (Math.random() * 250), a));
        g2.drawString("STMIK LIKMI", 180 + x, 280 + y);
    }

}
