/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafkom_2016130023;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Ellipse2D;
import java.io.File;
import java.util.Random;
import java.util.Scanner;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 *
 * @author Muhammad Supian
 */
public class LatihanUI3 extends JPanel implements KeyListener {

    private Random r;
    private Ellipse2D.Float bidak, apel;
    private Ellipse2D.Float[] red;
    private int x, y, px, py, score = 0, n = 300;
    private int[] pxx, pyy;
    private final static int SIZE = 20;
    private char[][] maze;

    public LatihanUI3() {
        addKeyListener(this);
        r = new Random();
        this.x = 25;
        this.y = 0;
        try {
            File f = new File("./maze.txt");
            Scanner s = new Scanner(f);
            this.maze = new char[33][];
            for (int i = 0; i < 33; i++) {
                this.maze[i] = s.nextLine().toCharArray();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        this.bidak = new Ellipse2D.Float(this.x * SIZE, this.y * SIZE, SIZE, SIZE);
        red = new Ellipse2D.Float[n];
        pxx = new int[n];
        pyy = new int[n];

        do {
            px = r.nextInt(49);
            py = r.nextInt(33);
        } while (maze[py][px] == '@');

        for (int i = 0; i < n; i++) {
            do {
                pxx[i] = r.nextInt(49);
                pyy[i] = r.nextInt(33);
            } while (maze[pyy[i]][pxx[i]] == '@');
        }

        this.apel = new Ellipse2D.Float(px * SIZE, py * SIZE, SIZE, SIZE);
        for (int i = 0; i < n; i++) {
            red[i] = new Ellipse2D.Float(pxx[i] * SIZE, pyy[i] * SIZE, SIZE, SIZE);
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D d = (Graphics2D) g;
        int x, y;
        for (y = 0; y < this.maze.length; y++) {
            for (x = 0; x < this.maze[y].length; x++) {
                if (this.maze[y][x] == '@') {
                    d.fillRect(x * SIZE, y * SIZE, SIZE, SIZE);
                }
            }
        }
        g.setColor(new Color(0, 0, 215));
        d.fill(bidak);
        g.setColor(Color.RED);
        d.fill(apel);
        g.setColor(new Color(255, 0, 0));
        for (int i = 0; i < n; i++) {
            d.fill(red[i]);
        }
        g.setColor(Color.WHITE);
        g.setFont(new Font("Consolas", Font.BOLD, 20));
        g.drawString(String.format("SCORE: %3d", score), 3, 16);
    }

    public void keyTyped(KeyEvent e) {
    }

    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == 38 && y > 0 && this.maze[y - 1][x] != '@') {
            y--;
        }
        if (e.getKeyCode() == 40 && y < 32 && this.maze[y + 1][x] != '@') {
            y++;
        }
        if (e.getKeyCode() == 37 && x > 0 && this.maze[y][x - 1] != '@') {
            x--;
        }
        if (e.getKeyCode() == 39 && x < 48 && this.maze[y][x + 1] != '@') {
            x++;
        }
        this.bidak.setFrame(x * SIZE, y * SIZE, SIZE, SIZE);
        if (y == py && x == px) {
            do {
                px = r.nextInt(49);
                py = r.nextInt(33);
            } while (maze[py][px] == '@');
            this.apel.setFrame(px * SIZE, py * SIZE, SIZE, SIZE);
            score += 5;
        }
        for (int i = 0; i < n; i++) {
            if (y == pyy[i] && x == pxx[i]) {
                do {
                    pxx[i] = r.nextInt(49);
                    pyy[i] = r.nextInt(33);
                } while (maze[pyy[i]][pxx[i]] == '@');
                red[i].setFrame(pxx[i] * SIZE, pyy[i] * SIZE, SIZE, SIZE);
                score++;
            }
        }
        repaint();
    }

    public void keyReleased(KeyEvent e) {

    }

}
