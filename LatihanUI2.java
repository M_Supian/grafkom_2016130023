/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafkom_2016130023;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JPanel;

/**
 *
 * @author Muhammad Supian
 */
public class LatihanUI2 extends JPanel implements MouseListener, MouseMotionListener {

    private Rectangle block, block1, block2, block3, block4, block5;
    private boolean flag, flag1, flag2, flag3, flag4, flag5;
    private int deltaX, deltaY;

    public LatihanUI2() {
        addMouseListener(this);
        addMouseMotionListener(this);
        this.block = new Rectangle(50, 50, 50, 50);
        this.block1 = new Rectangle(150, 50, 50, 50);
        this.block2 = new Rectangle(250, 50, 50, 50);
        this.block3 = new Rectangle(50, 150, 50, 50);
        this.block4 = new Rectangle(150, 150, 50, 50);
        this.block5 = new Rectangle(250, 150, 50, 50);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        if (flag == false) {
            g2.setColor(Color.black);
        } else {
            g2.setColor(Color.magenta);
        }

        g2.fill(this.block);
        g2.fill(this.block1);
        g2.fill(this.block2);
        g2.fill(this.block3);
        g2.fill(this.block4);
        g2.fill(this.block5);
    }

    public void mouseClicked(MouseEvent e) {
        this.flag = !this.flag;
        repaint();
    }

    public void mousePressed(MouseEvent e) {

        int x = e.getX();
        int y = e.getY();
        if (this.block.contains(x, y)) {
            this.deltaX = x - (int) this.block.getX();
            this.deltaY = y - (int) this.block.getY();
            this.flag = true;
        }
        if (this.block1.contains(x, y)) {
            this.deltaX = x - (int) this.block1.getX();
            this.deltaY = y - (int) this.block1.getY();
            this.flag1 = true;
        }
        if (this.block2.contains(x, y)) {
            this.deltaX = x - (int) this.block2.getX();
            this.deltaY = y - (int) this.block2.getY();
            this.flag2 = true;
        }
        if (this.block3.contains(x, y)) {
            this.deltaX = x - (int) this.block3.getX();
            this.deltaY = y - (int) this.block3.getY();
            this.flag3 = true;
        }
        if (this.block4.contains(x, y)) {
            this.deltaX = x - (int) this.block4.getX();
            this.deltaY = y - (int) this.block4.getY();
            this.flag4 = true;
        }
        if (this.block5.contains(x, y)) {
            this.deltaX = x - (int) this.block5.getX();
            this.deltaY = y - (int) this.block5.getY();
            this.flag5 = true;
        }
        if (!this.block.contains(e.getX(), e.getY())) {
            return;
        }

    }

    public void mouseReleased(MouseEvent e) {
        this.flag = false;
        this.flag1 = false;
        this.flag2 = false;
        this.flag3 = false;
        this.flag4 = false;
        this.flag5 = false;
    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }

    public void mouseDragged(MouseEvent e) {
        if (flag == true) {
            int x = e.getX();
            int y = e.getY();
            int newX = x - this.deltaX;
            int newY = y - this.deltaY;
            if ((newX >= 0 && newX <= this.getWidth() - 50)
                    && (newY >= 0 && newY <= this.getHeight() - 50)) {
                this.block.setLocation(newX, newY);
            }
            repaint();
        }
        if (flag1 == true) {
            int x = e.getX();
            int y = e.getY();
            int newX = x - this.deltaX;
            int newY = y - this.deltaY;
            if ((newX >= 0 && newX <= this.getWidth() - 50)
                    && (newY >= 0 && newY <= this.getHeight() - 50)) {
                this.block1.setLocation(newX, newY);
            }
            repaint();
        }
        if (flag2 == true) {
            int x = e.getX();
            int y = e.getY();
            int newX = x - this.deltaX;
            int newY = y - this.deltaY;
            if ((newX >= 0 && newX <= this.getWidth() - 50)
                    && (newY >= 0 && newY <= this.getHeight() - 50)) {
                this.block2.setLocation(newX, newY);
            }
            repaint();
        }
        if (flag3 == true) {
            int x = e.getX();
            int y = e.getY();
            int newX = x - this.deltaX;
            int newY = y - this.deltaY;
            if ((newX >= 0 && newX <= this.getWidth() - 50)
                    && (newY >= 0 && newY <= this.getHeight() - 50)) {
                this.block3.setLocation(newX, newY);
            }
            repaint();
        }
        if (flag4 == true) {
            int x = e.getX();
            int y = e.getY();
            int newX = x - this.deltaX;
            int newY = y - this.deltaY;
            if ((newX >= 0 && newX <= this.getWidth() - 50)
                    && (newY >= 0 && newY <= this.getHeight() - 50)) {
                this.block4.setLocation(newX, newY);
            }
            repaint();
        }
        if (flag5 == true) {
            int x = e.getX();
            int y = e.getY();
            int newX = x - this.deltaX;
            int newY = y - this.deltaY;
            if ((newX >= 0 && newX <= this.getWidth() - 50)
                    && (newY >= 0 && newY <= this.getHeight() - 50)) {
                this.block5.setLocation(newX, newY);
            }
            repaint();
        }
    }

    public void mouseMoved(MouseEvent e) {

    }
}
